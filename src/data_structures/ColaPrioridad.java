package data_structures;

import java.util.Iterator;

import logic.CapacidadException;

public class ColaPrioridad<T extends Comparable<T>> implements IPriorityList<T>, Iterable<T>
{
	private Object[] colaPrioridad;

	int numElementos;

	public ColaPrioridad(int prioridadMax) 
	{
		colaPrioridad = new Object [prioridadMax];
		numElementos =0;		
	}


	@Override
	public int darNumeroElementos() 
	{
		return numElementos;
	}

	@Override
	public void agregar(T elemento) throws CapacidadException
	{
		if(numElementos == tamanoMax()) {
			throw new CapacidadException("El heap ya tiene la capacidad máxima de elementos");
		}
		
		colaPrioridad[numElementos++]=((T) elemento);

	}

	@SuppressWarnings("unchecked")
	@Override
	public T max() 
	{
		int max= 0;
		for(int i=1; i<numElementos; i++)
		{
			if(less(max,i))
			{
				max = i;
			}
			exch(max, numElementos-1);
		}
		return (T) colaPrioridad[--numElementos];
	}

	@Override
	public boolean esVacia()
	{
		return numElementos ==0 ;
	}

	@Override
	public int tamanoMax()
	{
		return colaPrioridad.length;
	}
	@SuppressWarnings("unchecked")
	private boolean less(int i, int j)
	{
		return ((T) colaPrioridad[i]).compareTo((T) colaPrioridad[j]) < 0;

	}
	private void exch(int i, int j)
	{
		Object t = colaPrioridad[i]; 
		colaPrioridad[i] = colaPrioridad[j]; 
		colaPrioridad[j] = t; 
	}


	@Override
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			T act = null;
			int pos= -1;

			@Override
			public boolean hasNext() {
				if (darNumeroElementos() == 0) {
					return false;
				}

				if (act == null) {
					return true;
				}

				return pos +1 < darNumeroElementos();
			}

			@Override
			public T next() {
				act = (T) colaPrioridad [++pos];
				return act;
			}
		};
	}


}
