package data_structures;

import logic.CapacidadException;

public class MaxHeapCP<T extends Comparable<T>> implements IPriorityList<T> {

	//------------------------------------------------------
	//Atributos
	//------------------------------------------------------

	private Object[] heap;

	private int numElementos;

	//------------------------------------------------------
	//Atributos
	//------------------------------------------------------

	public MaxHeapCP(int max) {
		heap = new Object[max + 1];
		numElementos = 0;
	}

	//------------------------------------------------------
	//Atributos
	//------------------------------------------------------

	@Override
	public int darNumeroElementos() {
		return numElementos;
	}

	@Override
	public void agregar(T elemento) throws CapacidadException {
		if(numElementos == tamanoMax()) {
			throw new CapacidadException("El heap ya tiene la capacidad máxima de elementos");
		}

		heap[++numElementos] = elemento;
		swim(numElementos);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T max() {
		T max = (T) heap[1];
		
		heap[1] = heap[numElementos];
		heap[numElementos] = null;
		numElementos --;
		sink(1);
		
		return max;
	}

	@Override
	public boolean esVacia() {
		return numElementos == 0;
	}

	@Override
	public int tamanoMax() {
		return heap.length;
	}

	@SuppressWarnings("unchecked")
	private void swim(int pos) {
		while(pos > 1 && ((T) heap[pos/2]).compareTo((T) heap[pos]) < 0) {
			T temp = (T) heap[pos/2];
			heap[pos/2] = heap[pos];
			heap[pos] = temp;

			pos /= 2;
		}
	}

	@SuppressWarnings("unchecked")
	private void sink(int pos) {
		while(2*pos <= numElementos) {
			int j = 2*pos;
			if(j < numElementos && ((T) heap[j]).compareTo((T) heap[j+1]) < 0) {
				j++;
			}
			
			if(((T) heap[pos]).compareTo((T) heap[j]) > 0) {
				break;
			}
			
			T temp = (T) heap[pos];
			heap[pos] = heap[j];
			heap[j] = temp;
			
			pos = j;
		}
	}
}
