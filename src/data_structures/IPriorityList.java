package data_structures;

import logic.CapacidadException;

public interface IPriorityList <T extends Comparable<T>>{

	int darNumeroElementos();
	
	void agregar(T elemento) throws CapacidadException;
	
	T max();
	
	boolean esVacia();
	
	int tamanoMax();
}
