package view;

import java.time.LocalDateTime;
import java.util.Scanner;

import Controller.Controller;

public class view {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();
			int number = 0;

			switch(option)
			{
				case 1:
					System.out.println("Se cargaron " + Controller.loadTrips() + " viajes.");
					break;
					
				case 2:
					System.out.println("Introduce el número de datos que deseas cargar: (E.j. 1000)");
					number = sc.nextInt();
					Controller.heapDeViajes(number);
					break;
					
				case 3:
					System.out.println("Introduzca el número de datos que deseas cargar: (E.j 1000)");
					number = sc.nextInt();
					Controller.colaPrioridad(number);
					break;
					
				case 4:
					Controller.maxHeap();
					break;
					
				case 5:
					Controller.colaPrioridadMax();
					break;
					
				case 6:	
					fin=true;
					sc.close();
					break;
					
				default:
					System.out.println("La opción no es válida");
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cree una nueva coleccion de viajes");
		System.out.println("2. Crear heap.");
		System.out.println("3. Crear cola de prioridad.");
		System.out.println("4. Prueba sacar máximos de heap.");
		System.out.println("5. Prueba sacar máximos de cola de prioridad.");
		System.out.println("6. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
	}
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	public static LocalDateTime convertirFecha(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	public static LocalDateTime convertirFechaSinSegundos(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 0;

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
	
}
