package VO;

public class VOBike implements Comparable<VOBike> {
	
	private int bikeId;
    private double totalDistance;

    public VOBike(int bikeId, double totalDistance) {
        this.bikeId = bikeId;
        this.totalDistance = totalDistance;
    }

    public int getBikeId() {
        return bikeId;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    
    public void addDistance(double totalDistance) {
    	this.totalDistance += totalDistance;
    }

	@Override
	public int compareTo(VOBike o) {
		if(totalDistance < o.getTotalDistance()) {
			return -1;
		} else if (totalDistance > o.getTotalDistance()) {
			return 1;
		} else {
			return 0;
		}
	}

}
