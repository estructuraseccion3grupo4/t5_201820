package VO;

import java.time.LocalDateTime;

public class VOStation implements Comparable<VOStation> {

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private int id;

	private String name;

	private String city;

	private double latitude;

	private double longitude;

	private int dpCapacity;

	private LocalDateTime date;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param id
	 * @param name
	 * @param city
	 * @param latitude
	 * @param longitude
	 * @param dpCapacity
	 * @param date
	 */
	public VOStation(int id, String name, String city, double latitude, double longitude, int dpCapacity, LocalDateTime date) {
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpCapacity = dpCapacity;
		this.date = date;
	}

	//-----------------------------------------------------------
	// Métodos
	//-----------------------------------------------------------

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @return the dpCapacity
	 */
	public int getDpCapacity() {
		return dpCapacity;
	}

	/**
	 * @return the date
	 */
	public LocalDateTime getDate() {
		return date;
	}

	@Override
	public int compareTo(VOStation o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
