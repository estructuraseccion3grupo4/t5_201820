package Controller;

import VO.VOTrip;
import data_structures.ColaPrioridad;
import data_structures.MaxHeapCP;
import logic.Manager;

public class Controller {

	private static Manager manager = new Manager();
	
	public static int darTamanoStack() {
		return manager.darTamanoStack();
	}
	
	public static int loadTrips() {
		return manager.loadTrips();
	}
	
	public static MaxHeapCP<VOTrip> heapDeViajes(int n) {
		return manager.heapDeViajes(n);
	}
	
	public static void maxHeap() {
		manager.maxHeap();
	}
	
	public static ColaPrioridad<VOTrip> colaPrioridad(int n) {
		return manager.colaPrioridad(n);
	}
	
	public static void colaPrioridadMax() {
		manager.colaPrioridadMax();
	}
}
