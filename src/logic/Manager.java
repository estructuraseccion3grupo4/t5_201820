package logic;

import data_structures.Stack;
import data_structures.ColaPrioridad;
import data_structures.IList;
import data_structures.List;
import data_structures.MaxHeapCP;
import view.view;
import view.view;

import java.io.FileReader;
import java.time.LocalDateTime;

import com.opencsv.CSVReader;

import VO.VOTrip;

public class Manager {

	public final static String Q3_TRIPS_FILE = "./data/Divvy_Trips_2017_Q3.csv";
	public final static String Q4_TRIPS_FILE = "./data/Divvy_Trips_2017_Q4.csv";

	private Stack<VOTrip> tripsStack;
	
	private ColaPrioridad<VOTrip> colaPrioridad;
	
	private MaxHeapCP<VOTrip> heap;
	
	public int darTamanoStack() {
		return tripsStack.size();
	}

	public int loadTrips() {
		try {
			tripsStack = new Stack<>();

			System.out.println("Cargando Q4...");
			
			CSVReader reader = new CSVReader(new FileReader(Q4_TRIPS_FILE));
			String[] nextLine;
			reader.readNext();

			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = null;

				LocalDateTime startTime = view.convertirFechaSinSegundos(nextLine[1].split(" ")[0], nextLine[1].split(" ")[1]);
				LocalDateTime endTime = view.convertirFechaSinSegundos(nextLine[2].split(" ")[0], nextLine[2].split(" ")[1]);

				if(nextLine[10].isEmpty() && nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], "", 0);

				} else if (nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], 0);

				} else {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], Integer.parseInt(nextLine[11]));
				}

				tripsStack.push(trip);
			}
			
			System.out.println("Q4 cargado.");
			System.out.println("Q3 cargando...");

			reader = new CSVReader(new FileReader(Q3_TRIPS_FILE));
			reader.readNext();

			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = null;

				LocalDateTime startTime = view.convertirFecha(nextLine[1].split(" ")[0], nextLine[1].split(" ")[1]);
				LocalDateTime endTime = view.convertirFecha(nextLine[2].split(" ")[0], nextLine[2].split(" ")[1]);

				if(nextLine[10].isEmpty() && nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], "", 0);

				} else if (nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], 0);

				} else {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], Integer.parseInt(nextLine[11]));
				}

				tripsStack.push(trip);
			}

			System.out.println("Q3 cargado.");
			
			reader.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return tripsStack.size();
	}

	public IList<VOTrip> muestraDeViajes(int n) {
		List<VOTrip> list = new List<>();

		for(int i = 0; i < n; i++) {
			list.add(tripsStack.pop());
		}

		System.out.println("Quedan " + tripsStack.size() + " viajes en la pila.");
		return list;
	}

	public MaxHeapCP<VOTrip> heapDeViajes(int n) {
		System.out.println(n);
		List<VOTrip> list = (List<VOTrip>) muestraDeViajes(n);
		long startTime = System.currentTimeMillis();
		heap = new MaxHeapCP<>(n);

		try {
			for(int i = 0; i < list.size(); i++) {
				heap.agregar(list.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("Heap: " + time);
		
		return heap;
	}
	
	public void maxHeap() {
		long startTime = System.currentTimeMillis();
		
		for(int i = 0; i < heap.darNumeroElementos(); i++) {
			heap.max();
			//System.out.println(trip.getFromStationName());
		}
		
		long endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("Heap Max: " + time);
	}
	
	public ColaPrioridad<VOTrip> colaPrioridad(int n)
	{
		List<VOTrip> list = (List<VOTrip>) muestraDeViajes(n);
		long startTime = System.currentTimeMillis();
		colaPrioridad = new ColaPrioridad<>(n);

		try {
			for(int i = 0; i < list.size(); i++) {
				colaPrioridad.agregar(list.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("Cola prioridad: " + time);
		
		return colaPrioridad;
	}
	
	public void colaPrioridadMax()
	{
		long startTime = System.currentTimeMillis();

		try {
			for(int i = 0; i < colaPrioridad.darNumeroElementos(); i++) {
				colaPrioridad.max();
				//System.out.println(trip.getFromStationName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("Cola prioridad: " + time);
	}
}
