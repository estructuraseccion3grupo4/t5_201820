package logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.Iterator;

import com.opencsv.CSVReader;

import VO.VOStation;
import VO.VOTrip;
import data_structures.List;
import view.DivvyTripsManagerView;
import view.view;

public class ManejadorBicicletas {

	public final static String STATIONS_FILE = "./data/Divvy_Stations_2017_Q3Q4.csv";

	private List<VOStation> stationsList;

	private void loadStations() {
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(STATIONS_FILE));

			String[] nextLine;
			reader.readNext();

			while ((nextLine = reader.readNext()) != null) {
				LocalDateTime time = view.convertirFecha(nextLine[6].split(" ")[0], nextLine[6].split(" ")[1]);

				VOStation station = new VOStation(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2],
						Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]),
						Integer.parseInt(nextLine[5]), time);

				stationsList.add(station);
			}
		} catch (Exception e) {

		}
	}

	private void inicializarDistancias() {
		Iterator<VOTrip> iterTrips = tripsStack.iterator();

		while(iterTrips.hasNext()) {
			VOTrip trip = iterTrips.next();

			Iterator<VOStation> iterStations = stationsList.iterator();

			while(iterStations.hasNext()) {
				VOStation station = iterStations.next();

				if(trip.getFromStationId() == station.getId()) {
					trip.setFromStationLatitud(station.getLatitude());
					trip.setFromStationLongitud(station.getLongitude());
				}

				if(trip.getToStationId() == station.getId()) {
					trip.setToStationLatitud(station.getLatitude());
					trip.setToStationLongitud(station.getLongitude());
				}
			}
		}
	}

}
