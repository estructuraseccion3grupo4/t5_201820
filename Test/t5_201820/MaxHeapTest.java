package t5_201820;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import data_structures.MaxHeapCP;
import logic.CapacidadException;

class MaxHeapTest {

	private MaxHeapCP<PruebaVO> heap;

	public void scenario1() {
		heap = new MaxHeapCP<>(5);
	}

	/*
	 * Todas tienen diferentes estaciones de inicio.
	 */
	public void scenario2() throws CapacidadException {
		heap = new MaxHeapCP<>(5);

		PruebaVO prueba1 = new PruebaVO("Estacion1", "Estacion2", 10);
		PruebaVO prueba2 = new PruebaVO("Estacion2", "Estacion2", 10);
		PruebaVO prueba3 = new PruebaVO("Estacion3", "Estacion2", 10);
		PruebaVO prueba4 = new PruebaVO("Estacion4", "Estacion2", 10);
		PruebaVO prueba5 = new PruebaVO("Estacion5", "Estacion2", 10);

		heap.agregar(prueba1);
		heap.agregar(prueba2);
		heap.agregar(prueba3);
		heap.agregar(prueba4);
		heap.agregar(prueba5);
	}
	
	/*
	 * Todas tienen diferentes estaciones finales.
	 */
	public void scenario3() throws CapacidadException {
		heap = new MaxHeapCP<>(5);

		PruebaVO prueba1 = new PruebaVO("Estacion1", "Estacion1", 10);
		PruebaVO prueba2 = new PruebaVO("Estacion1", "Estacion2", 10);
		PruebaVO prueba3 = new PruebaVO("Estacion1", "Estacion3", 10);
		PruebaVO prueba4 = new PruebaVO("Estacion1", "Estacion4", 10);
		PruebaVO prueba5 = new PruebaVO("Estacion1", "Estacion5", 10);

		heap.agregar(prueba1);
		heap.agregar(prueba2);
		heap.agregar(prueba3);
		heap.agregar(prueba4);
		heap.agregar(prueba5);
	}
	
	/*
	 * Todas tienen diferentes duraciones.
	 */
	public void scenario4() throws CapacidadException {
		heap = new MaxHeapCP<>(5);

		PruebaVO prueba1 = new PruebaVO("Estacion1", "Estacion1", 10);
		PruebaVO prueba2 = new PruebaVO("Estacion1", "Estacion1", 20);
		PruebaVO prueba3 = new PruebaVO("Estacion1", "Estacion1", 30);
		PruebaVO prueba4 = new PruebaVO("Estacion1", "Estacion1", 40);
		PruebaVO prueba5 = new PruebaVO("Estacion1", "Estacion1", 50);

		heap.agregar(prueba1);
		heap.agregar(prueba2);
		heap.agregar(prueba3);
		heap.agregar(prueba4);
		heap.agregar(prueba5);
	}

	@Test
	void testDarNumeroElementos() {
		scenario1();

		assertEquals(0, heap.darNumeroElementos(), "No debería haber ningún elemento");


		try {
			scenario2();
			assertEquals(5, heap.darNumeroElementos(), "El número de elementos no corresponde");
		} catch(Exception e) {
			fail("No debería generar excepción");
		}
	}

	@Test
	void testAgregar() {
		try {
			scenario2();
			
			assertEquals(5, heap.darNumeroElementos(), "El número de elementos no corresponde");

			try {
				heap.agregar(new PruebaVO("", "", 0));
				fail("Debería generar una excepción");
			} catch(Exception e) {

			}

		} catch (Exception e) {
			fail("No debería generar excepción");
		}
	}

	@Test
	void testMax() {
		try {
			//Escenario 1 (Diferentes estaciones iniciales)
			scenario2();
			ArrayList<PruebaVO> pruebas = new ArrayList<>();

			PruebaVO temp = heap.max();
			while(temp != null) {
				pruebas.add(temp);
				temp = heap.max();
			}
			assertEquals(5, pruebas.size(), "No se están sacando todos los elementos");
			
			for(int i = 0; i < pruebas.size() - 1; i++) {
				System.out.println(pruebas.get(i));
				if(pruebas.get(i).compareTo(pruebas.get(i+1)) < 0) {
					fail("No está ordenado el heap");
				}
			}
			
			//Escenario 2 (Diferentes estaciones finales)
			scenario3();
			pruebas = new ArrayList<>();

			temp = heap.max();
			while(temp != null) {
				pruebas.add(temp);
				temp = heap.max();
			}
			assertEquals(5, pruebas.size(), "No se están sacando todos los elementos");
			
			for(int i = 0; i < pruebas.size() - 1; i++) {
				System.out.println(pruebas.get(i));
				if(pruebas.get(i).compareTo(pruebas.get(i+1)) < 0) {
					fail("No está ordenado el heap");
				}
			}
			
			//Escenario 3 (Diferentes duraciones)
			scenario4();
			pruebas = new ArrayList<>();

			temp = heap.max();
			while(temp != null) {
				pruebas.add(temp);
				temp = heap.max();
			}
			assertEquals(5, pruebas.size(), "No se están sacando todos los elementos");
			
			for(int i = 0; i < pruebas.size() - 1; i++) {
				System.out.println(pruebas.get(i));
				if(pruebas.get(i).compareTo(pruebas.get(i+1)) < 0) {
					fail("No está ordenado el heap");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("No debería generar excepciones");
		}
	}

	@Test
	void testEsVacia() {
		scenario1();
		assertTrue(heap.esVacia(), "La lista está vacía");

		try {
			scenario2();
			assertFalse(heap.esVacia(), "La lista no está vacía");
		} catch(Exception e) {
			fail("No debería generar una excepción");
		}
	}
}
