package t5_201820;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import data_structures.ColaPrioridad;
import logic.CapacidadException;


class ColaPrioridadTest 
{
	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	private ColaPrioridad<PruebaVO> list;

	//-----------------------------------------------------------
	// Scenarios
	//-----------------------------------------------------------

	void setupScenario0()
	{
		list = new ColaPrioridad<>(3);
	}
	/**
	 * Todas tienen diferentes estaciones de inicio
	 * @throws CapacidadException
	 */

	void setupScenario1() throws CapacidadException
	{
		list = new ColaPrioridad<>(3);

		PruebaVO prueba1= new PruebaVO("Estacion 1", "Estacion 2", 10);
		PruebaVO prueba2= new PruebaVO("Estacion 2", "Estacion 2", 10);
		PruebaVO prueba3= new PruebaVO("Estacion 3", "Estacion 2", 10);

		list.agregar(prueba1);
		list.agregar(prueba2);
		list.agregar(prueba3);
	}
	void setupScenario2() throws CapacidadException
	{
		list = new ColaPrioridad<>(3);

		PruebaVO prueba1= new PruebaVO("Estacion 1", "Estacion 2", 10);
		PruebaVO prueba2= new PruebaVO("Estacion 1", "Estacion 3", 10);
		PruebaVO prueba3= new PruebaVO("Estacion 1", "Estacion 4", 10);

		list.agregar(prueba1);
		list.agregar(prueba2);
		list.agregar(prueba3);
	}
	void setupScenario3() throws CapacidadException
	{
		list = new ColaPrioridad<>(3);

		PruebaVO prueba1= new PruebaVO("Estacion 1", "Estacion 1", 10);
		PruebaVO prueba2= new PruebaVO("Estacion 1", "Estacion 1", 20);
		PruebaVO prueba3= new PruebaVO("Estacion 1", "Estacion 1", 30);

		list.agregar(prueba1);
		list.agregar(prueba2);
		list.agregar(prueba3);
	}

	@Test
	void testDarNumeroElementos() 
	{
		setupScenario0();
		assertEquals(0, list.darNumeroElementos(), "El numero de elementos deberia ser 0");
		try
		{
			setupScenario1();
			assertEquals(3, list.darNumeroElementos(), "El numero de elemenetos es incorrecto");
			setupScenario2();
			assertEquals(3, list.darNumeroElementos(),"El numero de elementos es incorrecto");

		}
		catch(Exception e)
		{
			fail("No deberia generar excepcion");
		}
	}

	@Test
	void testAgregar() 
	{
		try
		{
			setupScenario0();
			list.agregar(new PruebaVO("Estacion 1","Estacion 2", 10));
			assertEquals(1, list.darNumeroElementos(), "Se agrego correctamente");
			
		}
		catch(Exception e)
		{
			fail("No deberia generar excepcion");
		}

	}

	@Test
	void testMax()
	{
		try
		{
			setupScenario1();
			ArrayList<PruebaVO> pruebas = new ArrayList<>();

			PruebaVO temp = list.max();
			while(temp != null) 
			{
				pruebas.add(temp);
				//System.out.println(temp);
				if(!list.esVacia()) 
				{
					temp = list.max();
				} 
				else 
				{
					break;
				}
			}

			assertEquals(3, pruebas.size(), "No se están sacando todos los elementos");

			for(int i = 0; i < pruebas.size() - 1; i++) 
			{
				System.out.println(pruebas.get(i).darEstacionInicio());
				System.out.println(pruebas.get(i+1).darEstacionInicio());
				if(pruebas.get(i).compareTo(pruebas.get(i+1)) < 0)
				{
					fail("No está ordenada la cola");
				}
			}
			setupScenario2();
			ArrayList<PruebaVO> pruebas1 = new ArrayList<>();

			PruebaVO temp1 = list.max();
			while(temp1 != null) 
			{
				pruebas1.add(temp1);
				//System.out.println(temp);
				if(!list.esVacia()) 
				{
					temp1 = list.max();
				} 
				else 
				{
					break;
				}
			}

			assertEquals(3, pruebas1.size(), "No se están sacando todos los elementos");

			for(int i = 0; i < pruebas1.size() - 1; i++) 
			{
				System.out.println(pruebas1.get(i).darEstacionInicio());
				System.out.println(pruebas1.get(i+1).darEstacionInicio());
				if(pruebas1.get(i).compareTo(pruebas1.get(i+1)) < 0)
				{
					fail("No está ordenada la cola");
				}
			}
			setupScenario3();
			ArrayList<PruebaVO> pruebas2 = new ArrayList<>();

			PruebaVO temp2 = list.max();
			while(temp2 != null) 
			{
				pruebas2.add(temp2);
				//System.out.println(temp);
				if(!list.esVacia()) 
				{
					temp2 = list.max();
				} 
				else 
				{
					break;
				}
			}

			assertEquals(3, pruebas2.size(), "No se están sacando todos los elementos");

			for(int i = 0; i < pruebas2.size() - 1; i++) 
			{
				System.out.println(pruebas2.get(i).darEstacionInicio());
				System.out.println(pruebas2.get(i+1).darEstacionInicio());
				if(pruebas2.get(i).compareTo(pruebas2.get(i+1)) < 0)
				{
					fail("No está ordenada la cola");
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Test
	void testEsVacia()
	{
		setupScenario0();

		assertTrue(list.esVacia(), "La cola de prioridad deberia estar vacia");
		try
		{
			setupScenario2();
			assertFalse(list.esVacia(), "La cola de prioridad no deberia estar vacia");
		} 
		catch (Exception e) 
		{
			fail("No deber�a generar errores");
		}
	}
}
