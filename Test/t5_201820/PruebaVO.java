package t5_201820;

public class PruebaVO implements Comparable<PruebaVO> {
	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private String estacionInicio;
	
	private String estacionFinal;
	
	private int duracion;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param estacionInicio
	 * @param estacionFinal
	 * @param duracion
	 */
	public PruebaVO(String estacionInicio, String estacionFinal, int duracion) {
		this.estacionInicio = estacionInicio;
		this.estacionFinal = estacionFinal;
		this.duracion = duracion;
	}

	//-----------------------------------------------------------
	// Métodos
	//-----------------------------------------------------------

	@Override
	public int compareTo(PruebaVO o) {
		if(estacionInicio.compareTo(o.darEstacionInicio()) == 0) {
			if(estacionFinal.compareTo(o.darEstacionFinal()) == 0) {
				
				if(duracion < o.darDuracion()) {
					return -1;
				} else if(duracion > o.darDuracion()) {
					return 1;
				} else {
					return 0;
				}
				
			} else if(estacionFinal.compareTo(o.darEstacionFinal()) < 0) {
				return -1;
			} else {
				return 1;
			}
			
		} else if(estacionInicio.compareTo(o.darEstacionInicio()) < 0) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * @return the estacionInicio
	 */
	public String darEstacionInicio() {
		return estacionInicio;
	}

	/**
	 * @return the estacionFinal
	 */
	public String darEstacionFinal() {
		return estacionFinal;
	}

	/**
	 * @return the duracion
	 */
	public int darDuracion() {
		return duracion;
	}
	
	
}
